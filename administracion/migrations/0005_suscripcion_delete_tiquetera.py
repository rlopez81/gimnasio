# Generated by Django 4.2.7 on 2023-11-14 02:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administracion', '0004_remove_user_tiquetera'),
    ]

    operations = [
        migrations.CreateModel(
            name='Suscripcion',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('comment', models.CharField(blank=True, max_length=200)),
                ('servicio', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='get_suscripciones', to='administracion.servicio')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='get_users', to='administracion.user')),
            ],
        ),
        migrations.DeleteModel(
            name='Tiquetera',
        ),
    ]

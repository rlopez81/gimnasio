"""
URL configuration for gym project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from administracion import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'Trainer_rest', views.TrainerViewSet)
router.register(r'User_rest',views.UserViewSet)
router.register(r'Servicio_rest',views.ServicioViewSet)
router.register(r'Suscripcion_rest',views.SuscripcionViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('trainer/', views.TrainerListView.as_view(), name='trainer-list'),
    path('trainer/<int:pk>/detail/',views.TrainerDetailView.as_view(), name='trainer-detail'),
    path('suscripcion/', views.SuscripcionListView.as_view(), name='suscripcion-list'),
    path('suscripcion/<int:pk>/detail/',views.SuscripcionDetailView.as_view(), name='suscripcion-detail'),
    path('user/', views.UserListView.as_view(), name='user-list'),
    path('user/<int:pk>/detail/',views.UserDetailView.as_view(), name='user-detail'),
    path('user/<int:pk>/update/',views.UserUpdate.as_view(),name='user-update'), 
    #Create
    path('user/create/', views.UserCreate.as_view(), name='user-create'),
    path('suscripcion/create/', views.SuscripcionCreate.as_view(), name='suscripcion-create'),
    path('servicio/create/', views.ServicioCreate.as_view(), name='servicio-create'),
    path('trainer/create/', views.TrainerCreate.as_view(), name='trainer-create'),
    #Delete
    path('user/<int:pk>/delete/', views.UserDelete.as_view(), name='user-delete'),
    path('suscripcion/<int:pk>/delete/', views.SuscripcionDelete.as_view(), name='suscripcion-delete'),
    path('servicio/<int:pk>/delete/', views.ServicioDelete.as_view(), name='servicio-delete'),
    path('trainer/<int:pk>/delete/', views.TrainerDelete.as_view(), name='trainer-delete'),
    path('trainer/<int:pk>/update/',views.TrainerUpdate.as_view(),name='trainer-update'), 

    path('servicio/', views.ServicioListView.as_view(), name='servicio-list'),
    path("search/", views.search, name="search"),


    path('servicio/<int:pk>/update/',views.ServicioUpdate.as_view(),name='servicio-update'), 

    path('',include(router.urls)),
    path('api/', include('rest_framework.urls',namespace = 'rest_framework')),
]
